---
layout: single
title: Über den Blog
permalink: /about/
author_profile: true
author: basti
---

Herzlich willkommen auf meinem Blog, der auf dem Webseiten-Generator [Jekyll](http://jekyllrb.com) basiert und im Hintergrund mit statischen Daten von meiner [Nextcloud](http://www.nextcloud.com)-Instanz mit neuem Inhalt gespeist wird. 

Sehr lange konnte ich mir nicht vorstellen einen Blog zu betreiben. "*Wozu auch*"? Was kann schon so interessant sein, dass man darüber große Texte schreiben sollte? 

### Was hat sich also geändert?  ###
Gar nicht so viel, außer vielleicht das ich älter geworden bin und dadurch einfach ein paar mehr Geschichten auf Lager habe[^bastelleidenschaft]. 
Und da es allgemein bekannt ist wie löchrig unsere Erinnerung mit der Zeit wird, dachte ich daher, halte ich ein paar Erinnerungen fest. Also warum nicht gleich öffentlich? Aber keine Angst:

### Es soll natürlich kein Tagebuch werden! ###

Aber es soll durchaus zum Nachdenken, Anregen sowie Weiterhelfen beitragen. Kategorisch würde ich vorweg nichts auschließen. Also folgende Themen sind mit Sicherheit zu erwarten:

- Es werden also Artikel für die Nerds unter euch erscheinen, wie ich z.B. diesen Blog online gestellt habe. Ziel ist natürlich, dass auch Laien diese Anleitungen dann Schritt für Schritt durchführen können.

- Ein weiteres sehr großes Thema für mich wird auch das Niederschreiben meiner Geschichten in Ozeanien und Europa sein, wo ich viel per Anhalter unterwegs war. Viele kennen Ausschnitte, kaum einer die ganzen Geschichten. Außerdem möchte ich über weitere Geschichten die man halt eher beim Reisen, also aus seinen Standardroutinen herausgerissen, erleben kann. 

- Und in Zukunft will ich mich auch ein wenig in der Philosophie probieren. Mal schauen ob ich etwas für mich zufriendenstellendes hinbekomme. :wink:

### Noch ein wenig zu mir ###

Die, die mich nicht kennen, lernen mich vielleicht dank des Inhalts dieses Blogs kennen und die mich bereits kennen, vielleicht noch ein wenig besser. :smiley:
Ansonsten bin ich für viele Späße offen und habe auch immer genügend Feuer im Arsch für neue Projekte. Bin oft am träumen und zu selten am rasten. Denn auch wenn wir bereits im Paradies leben[^keine_verasche], kann man noch **vieles** besser machen. 

Das wars erstmal...  

[^bastelleidenschaft]: und ja ich geb es ja zu, meine Bastelleidenschaft wollte gestillt werden
[^keine_verasche]: Ja das ist mein Ernst!