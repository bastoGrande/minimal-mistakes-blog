My blog is based on the following technologies:
- [Jekyll](https://github.com/jekyll/jekyll) (4.1.1)
- [Minimal Mistakes Jekyll Theme](https://github.com/mmistakes/minimal-mistakes/) (4.19.3)
- [Font Awesome Icons](https://fontawesome.com) (5.5.0)
- [bigfoot.js](https://github.com/lemonmade/bigfoot), [Mapbox integration](https://github.com/mapbox), ..
