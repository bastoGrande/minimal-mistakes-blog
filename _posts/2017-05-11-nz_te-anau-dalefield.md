---
layout: single
excerpt: "Ein erster Ausschnitt aus meinen Abenteuern auf der Südhalbkugel in einem spektakulären Teil Neuseelands zwischen Fjorden und Alpen. Zum Ende hin noch ein paar nachträgliche Gedankengänge."
title:  "Neuseeland: Te Anau -> Dalefield"
date:   2017-05-11 12:00:00 +0100
author_profile: true
author: basti
tags: Reisen Neuseeland per-Anhalter tolle-Menschen
header:
  image: "/assets/images/posts/te_anau-dalefield/header.jpg"
  caption: "Am südlichen Fuße der neuseeländischen Alpen. **© Bastian Gülstorf**"
---

Ein erster Ausschnitt aus meinen Abenteuern auf der Südhalbkugel in einem spektakulären Teil Neuseelands zwischen Fjorden und Alpen. Zum Ende hin noch ein paar nachträgliche Gedankengänge.

{% include toc %}

# Wann, wo, wieso? #
Ende März 2012, das ist also einer meiner ersten Herbsttage, hier auf der Südhalbkugel zwischen den sagenumwobenen Fjorden in Te Anau und den mächtigen und stolzen Alpen Neuseelands. Ein trostlos wirkender Ort, denn die Fjorde im bereits weitentfernten Westen fangen den meisten Regen ab, sodass hier auf der eher östlichen Seite der Insel nur wenig gedeiht[^Titelbild]. Die Straße in luftige Höhen scheint geradezu perfekt vor meinen Füßen ausgerollt. Selbst hier kommt es einen vor als ob jemand Staub gesaugt hätte, fast schon zu perfekt. Das Wetter ist sonnig und trocken, zwar nicht mehr heiß aber im Vergleich zum deutschen nassen Herbst doch sehr angenehm.  
Wieder einmal bin ich weitergezogen, wieder einmal will ich neues entdeckten und versuchen noch mehr neues aufzusaugen jedoch dabei auch immer bereit sein sich das schönste herauszufiltern, denn alle Eindrücke speicher ich zu diesem Zeitpunkt schon lange nicht mehr ab. Dafür reicht die Kapazität meines mit Glück und wundervollen Erinnerungen pralles, quasi zum Platzen volles Gedächtnis einfach nicht mehr aus.  
Alles was ich seit Monaten mit mir herumtrage ist gut verpackt in meinem großen Reiserucksack der mit mir aufrecht am Straßenrand darauf wartet, dass der nächste Windzug nicht einfach wieder nur durch mein inzwischen langes Haar weht, sondern jeden Augenblick eine weitere neue und aufregende Mitfahrgelegenheit aus dem Nichts präsentiert. 

# Seltsamer Start! #
Sicher bin ich mir natürlich nicht mehr aber es waren glaub ich runde 20 Minuten an dieser einsamen Stelle[^favs], bis ich meine üblichen Fragen[^fragen] am Fenster des vor mir haltenden Wagens wie aus dem Effeff gemächlich dem älteren Fahrer runterbeten konnte. Als ich dann saß und bevor er weiterfahren wollte, gab es erstmal eine für mich ungewöhnliche Einführung seinerseits, die ich nur noch grob wiedergeben kann:

>  "Ich kann dich leider nicht sehr weit mitnehmen. Mein Name ist **X**[^name_drop] und wenn du quatschen magst, können wir das gerne machen aber ich hab auch kein Problem wenn du eher ein wenig Ruhe für dich brauchst. Hier in der Mittelkonsole liegt ein Fotoalbum mit Erinnerungen aus meinem Leben, falls du Interesse hast." 
 
Also war klar das es eine kurze Mitfahrgelegenheit wird aber mir war es recht ein paar Kilometer weiter zu kommen und ein wenig zu plaudern, denn das Warten auf eine Mitfahrgelegenheit empfinde ich zwar nicht als unangenehm aber natürlich ist es auch kein riesen Spaß von dem man mich losreißen müsste.  
Die Distanzrekorde auf die ich in Australien noch so scharf war, waren in Neuseeland erstens gar nicht möglich, zweitens hatte ich kein Bedarf mehr daran, weil ich drittens schlicht auch einfach mehr das Mitfahrerlebnis genießen wollte.    

{% include maps/te_anau-dalefield.html %}

# Langsam wird es interessant.. #
Nach seiner seltsamen Einführung wollte ich dann natürlich nicht ausspannen[^relax], sondern herausfinden warum er mir das erzählte. Wir kamen also ins Gespräch und es stellte sich heraus, dass er 80 Jahre alt war und zehn Jahre zuvor mit seiner Frau nach Neuseeland gekommen war um ihren Sohn zu besuchen. Da die beiden Briten waren und Neuseeland ein [Commonwealth-Realm](https://de.wikipedia.org/wiki/Commonwealth_Realm "Commonwealth Realm") ist konnten sie ohne zu große Bürokratie bleiben und ihre Heimat damit hinter sich lassen.  
Leider ist seine Frau nach fünf Jahren verstorben. Jetzt könnte man denken, in so einem Alter in einem fremden Land jemanden so wichtiges zu verlieren kann eigentlich nur bedeuten: soziale Isolation[^isolation] oder die Rückkehr nach England um bei Familie/Freunden Hilfe zu suchen.  
Keines der beiden Optionen war ihm recht, nein im Grunde hat er eine 180° Wendung vollzogen und die Flucht nach vorne angetreten. Erst half er als Aushilfs-Sheriff in seiner neuen Heimat aus, was sich dann aber schnell als nicht die wahre Erfüllung herausgestellt hatte. Seine zweite und bis dahin endgültige Idee war ihm, ich glaube, eher zufällig gekommen. 

# Eine Idee die einen starken Eindruck auf mich macht
Vielen wird in diesem Moment vielleicht erstmal etwas negatives in den Sinn geschossen kommen aber keine Angst er hat keine Lust Anhalter mitzunehmen um im Anschluss irgendwas dummes mit ihnen anzustellen[^bad_feelings]. Nein viel einfacher, er beschloss ganz einfach jeden Tag um seinen Heimatort in Neuseeland herumzufahren und nach Anhaltern Ausschau zu halten um ihnen zu helfen.  
Ein netter Seiteneffekt war halt die regelmäßige Gesellschaft und sind nicht die Geschichten von Reisenden, also Geschichten die einen manchmal die Augen öffnen, manchmal die eigene Angst vergessen lassen oder oft ganz einfach helfen den Alltag zu vergessen, die beste Medizin gegen den tristen Einheitsbrei?  
Nach diesem ersten Wow-Moment saß ich also erstmal sprachlos da und bewunderte die spektakuläre an uns vorbeirauschende Voralpenlandschaft. Natürlich hab ich dann gerne und großzügig von meiner Reise und meinen Beweggründen erzählt und ihm glaub ich auch erstmal selber ein wenig Angst genommen. Auf die Frage ob er schon mal ein Problem hatte mit Anhaltern, kam prompt ein klares und kurzes: 

> **No!**

{% include figure image_path="/assets/images/posts/te_anau-dalefield/garston.jpg" id="garston" alt="Ein Platzhalter, falls das Bild nicht gefunden wird." caption="Garston, die isolierteste Gemeinde Neuseelands, jedenfalls in 2012. **©** Bastian Gülstorf" %}

# Und als wäre das alles noch nicht genug #
Er hatte es wahrlich verstanden seinen Mitreisenden allen möglichen Komfort zu bieten und zum Glück fiel mir die Mittelkonsole noch wieder rechtzeitig ein. Ich fragte nochmal kurz höflich ob ich ein Blick wagen darf, was nur mit einem vielsagendem Blick quittiert wurde.  
Im Grunde waren es nur Bilder von seiner Arbeit. Aber was für Bilder! Und vor allem wo! Sicher bin ich mir zwar nicht mehr wo genau in Afrika die Bilder aufgenommen worden sind aber ich glaube es war eher im südlichen Teil des Kontinents, vielleicht die Serengeti. Auf den Bildern hab ich auch seine Frau sehen können, also beide glücklich beim Pflegen von Wildtieren, sodass diese im Nachhinein wieder in die Wildnis entlassen werden konnten.  
Ich hab leider vergessen wie lange er dort gelebt hatte und warum sie beide nicht da geblieben sind. Aber wie eingangs bereits geschrieben, es war leider keine stundenlange Mitfahrt sondern lediglich eine halbe bis dreiviertel Stunde.  
Im Ort wo er mich dann rauslassen wollte und mir eine perfekte Stelle zum weiterfahren zeigte, schien ihm der schnelle Abschied dann doch nicht zuzusagen und er lud mich kurzerhand auf einen Kuchen ein. Nachdem er mir das mitgeteilt hatte, entgegnete ich ihm nur: 

> No please, it's time for a payback![^translation]

Ich hab es dann doch gelassen, nachdem er mir knapp und sehr deutlich mit einem "NO!!" und einem klaren Gesichtsausdruck zu verstehen gab, dass ich keine Chance hatte ihn umzustimmen. 
Wir erzählten also nochmal ein paar Minuten gemütlich zuammensitzend bevor er dann in seinen Wagen stieg auf der Suche nach neuen Anhaltern und ich mich an die Straße stellte auf der Suche nach neuen Mitfahrgelegenheiten/Abenteuern. Übrigens war es tatsächlich eine quasi perfekte Stelle zum Trampen:

* breite Spur neben der Fahrbahn zum Anhalten
* innerorts, also Vorbeifahrende müssen langsam fahren
* verlassen genug und wenig Verkehr um hilflos zu erscheinen
* ~~schlechtes Wetter -> noch hilfloser...~~

# Bevor es weitergeht noch ein wenig Zeit zum Nachdenken #
Dennoch hatte ich in Ozeanien nie länger als an dieser Stelle gewartet. Satte drei Stunden waren es am Ende aber hey: ich konnte die drei Stunden perfekt nutzen um das zuvor Geschehene einzuordnen und zu reflektieren. Mir wurde bewusst was für ein Glück ich hatte, genau an dieser extrem isolierten Stelle Neuseelands, siehe Bild von Garston, den wahrscheinlich einzigen nach Anhaltern suchenden Menschen Neuseelands[^lonely_place] zu finden.  
Jetzt im Nachhinein, wurde mir zusätzlich bewusst was für eine wertvolle Lektion ich damals gelernt hatte. Zu sehen das egal wie alt/jung oder wie reich/arm[^oder_or_ou] jemand ist, ohne soziale Kontakte funktionieren wir Menschen nicht!  
Also an alle die bis hierhin durchgehalten haben, hinterfragt euch doch mal, wen aus eurem Bekannten-, Freundes- oder Familienkreis habt ihr lange keinen Besuch mehr abgestattet und macht nicht nur der Person eine Freude, sondern auch euch selber.
Eine letzte Anekdote noch. Ich glaube er erzählte mir davon beim Kuchen, dass er am vorherigen Tag sehr traurig war. Ich wollte natürlich wissen warum und als Antwort kam schlicht: 

> Ich habe gestern niemand helfen können[^traurig]

[^Titelbild]: Wie man auf der eher rechten Halbseite des Titelbildes erkennen kann.
[^favs]: Meine Favoriten :wink:
[^fragen]: What direction? May I enter? ...
[^name_drop]: Extrem schade das ich ihn vergessen habe!
[^relax]: Es war eh noch morgens und außer aufstehen und bis zu dieser Stelle trampen hatte ich bis dahin ja noch kein Stress :smiley:
[^isolation]: Als vermeintlichen Schutz.
[^bad_feelings]: Oder sie gar zu töten.
[^translation]: Übersetzung: Nein, bitte dieses Mal möchte ich bezahlen.
[^lonely_place]: Vielleicht sogar Ozeaniens.
[^oder_or_ou]: Oder oder oder...
[^traurig]: Weil er niemand auf der Straße zum Mitnehmen gefunden hatte.
