---
layout: single
excerpt: Eine Idee die das Reisen revolutionieren sollte aber nicht nur für den Reisenden. Ein Beitrag über die Idee und meine Erfahrungen über das Besuchen und Besucht werden von Fremden, die oft zu Bekannten werden und manchmal sogar zu Freunden.
title:  "Couchsurfing"
date:   2017-07-03 12:00:00 +0100
author_profile: true
author: basti
tags: Couchsurfing Reisen tolle-Menschen
header:
  image: "/assets/images/posts/couchsurfing/header.jpg"
  caption: "Eine Couchsurfing Nacht direkt am Strand der Insel Java, Indonesien. **© Bastian Gülstorf**"
---
Eine Idee die das Reisen revolutionieren sollte aber nicht nur für den Reisenden. Ein Beitrag über die Idee und meine Erfahrungen über das Besuchen und Besucht werden von Fremden, die oft zu Bekannten werden und manchmal sogar zu Freunden.

{% include toc %}
 
# Warum überhaupt #
Wann ich das erste Mal von Couchsurfing (CS) gehört habe, weiß ich nicht mehr. Lediglich wann ich das erste Mal mich eingehender mit der Idee beschäftigt hatte. Ich wohnte zu der Zeit noch in Hamburg und war mit einer Freundin in der Innenstadt unterwegs als mich auf einmal ein Buch in einem Schaufenster allein vom Titel ansprach. Nach kurzem Zögern kaufte ich mir das Buch. In dem Buch beschreibt ein Mann wie er ohne Geld von Berlin bis zur Antarktis mit Umwegen über Kanada und Hawaii gereist war. Was von dem Buch am meisten in Erinnerung blieb, waren seine Beschreibungen zu seinen CS-Unterkünften[^hitchhiking]. Beeindruckt von den tollen Geschichten hab mich dann weiter zu CS im Internet belesen und mich schließlich angemeldet. 

# Grundlegendes #
Für Leser die CS nicht kennen: CS ist ein Online Portal bei dem man sich, wie zum Beispiel bei Facebook, ein Profil erstellen kann um es anschließend mit persönlichen Daten zu befüllen. Sobald man ein Profil hat, kann man im Grunde bereits bei anderen Mitgliedern, die eine Schlafmöglichkeit anbieten, eine Anfrage stellen ob man bei Ihnen **umsonst** übernachten kann. Ich muss gestehen, dass ich selber anfangs naiv dachte unbedingt eine Couch haben zu müssen. Das ist natürlich quatsch, denn jeder Schlafplatz ist möglich wie z.B. Couch, Bett, Boden, Hängematte, ...  
Umsonst, ja richtig umsonst! Es gibt daher auch viele Gäste die z.B. gerne ein Gericht aus ihrer Heimat (sei aus einem anderen Land oder einer Nachbarstadt) kochen, um sich für die kostenlose Unterkunft zu bedanken[^no_duty].  
Ähnlich verhält es sich für den/die Gastgeber*in, wenn er/sie genügend Vertrauen in den Gast hat, kann es durchaus vorkommen, dass dem Gast sogar ein Haus- oder Wohnungsschlüssel anvertraut oder der Inhalt des Kühlschranks geteilt wird oder oder oder..   
Bezüglich des persönlichen Umgangs gibt es natürlich die üblichen Benimm-Regeln. Wer längere und persönlichere und dementsprechend netter zu lesende Anfragen stellt und sich als Gast so verhält wie man selber gerne Gäste empfangen möchte, hat natürlich bessere Chancen auf eine kostenlose Unterkunft. Viele, wie ich auch, achten zusätzlich noch ob man vielleicht gemeinsame Interessen mit dem Gastgeber hat, da es ganz simpel die Barrieren beim Kennenlernen einreißt. Ein einfaches Beispiel wären zwei Fußballfreunde, der eine aus Brasilien und der andere aus Deutschland. Was wäre da nicht naheliegender als ein wahrscheinlich stundenlanges Gespräch über das "7:1"[^seteEum], sei es mit unterschiedlichen oder sogar gleichen Ansichten.

{% include figure image_path="/assets/images/posts/couchsurfing/couch.jpg" id="couch" alt="Ein Platzhalter, falls das Bild nicht gefunden wird." caption="Keine CS-Unterkunft aber so könnte es aussehen, Melbourne 2012. **©** Bastian Gülstorf" %}

# Mein erster (realer) Kontakt #
Natürlich hatte ich anfangs egoistische Beweggründe. Wenn ich zum Beispiel selber woanders unterkommen wollte, dachte ich, wäre es wohl besser bereits Referenzen zu haben[^still_exist]. Ich öffnete also erstmal mein Zuhause indem ich mir ein Profil erstellte.  
Meine erste Anfrage erhielt ich von einem rumänischen Pärchen, das in Wien zusammen Musik oder genauer gesagt das Violine-Spiel studiert hat. Leider habe ich die Anfrage nicht mehr aber es müssen wundervolle Zeilen meines ersten persönlichen Kontaktes mit CS gewesen sein. Zusätzlich waren glaub ich die Profile beider informativ sowie sympathisch, sodass ich tatsächlich einfach alle weiteren Bedenken bei Seite legen konnte und sie dementsprechend in meine Wohnung eingeladen hab.  
Bis heute hab ich nie wieder ein schöneres Erlebnis als Gastgeber gehabt! Neben unzähligen Gesprächen bei Wein, Kaffee oder auch Tee durfte ich Zeuge eines live-Mini-Konzerts werden. Ich hatte Bedenken, dass aufgrund der Uhrzeit und der Lautstärke, irgendein Nachbar um mich herum mittels lautem Klopfen oder Sturmklingeln es vorzeitig beenden würde. Es passierte nichts dergleichen und so saß ich also beeindruckt von der Wucht des Moments entspannt in meinem Sessel und genoss mit geschlossenen Augen und einem breiten Grinsen dieses exklusive Geschenk ihrerseits an mich.  
Als wäre das noch nicht genug, durfte ich zusätzlich eines der leckersten Gerichte meines Lebens genießen. Es steckte voller Dankbarkeit! So wie man die Liebe schmecken kann, konnte ich in dem Moment deren ehrliche und liebevolle Zubereitung bestens herausschmecken. Ich weiß nur noch, dass es ein traditionelles Fischgericht[^special_dish] war und ich es fast verschlungen habe, so lecker war es. Vier Nächte waren die beiden am Ende bei mir, es waren vier Nächte, die mein Leben ändern sollten.

# Vorteile des Gastgebers #
Die nächsten Gäste kamen aus Litauen, Luxemburg, Israel, Argentinien, China, Polen, Australien und ja auch aus Deutschland. Wenn man sich die Liste nochmal anschaut, fällt auf das fast von jedem Kontinent ein Gast bei mir auf der Couch war. Das ist zugleich eine der größten Vorteile des Beherrbergens, denn egal ob man selber vielleicht nicht die ganze Welt bereisen kann, sei es aufgrund zeitlicher oder monetärer Natur, man braucht es ja auch gar nicht. Da man die Welt zu sich nach Hause einladen kann. Zumindest kann man einen Eindruck auf die fremde Kultur des Gastes erahnen, denn um sie ganz zu erfassen muss man schon vorort sein. Beispielsweise könnte jemand der aus einem tropischen Land kommt, der, ohne Kälte kennengelernt zu haben, aufgewachsen ist, bei einem Besuch in einem kalten Land im Winter doch nicht einfach so, seine tropische Lebenslust zeigen. Für die benötigt man halt die heiße tropische Sonne des Tages und eine stets warme tropische Nacht um gemütlich sein Leben zu genießen.  
Es gibt noch einen weiteren positiven Nebeneffekt beim Öffnen des Heims. Man glaubt ja oft sich in seiner Heimat, sei es nun eine Metropole oder ein Dorf, bestens auszukennen. Das mag auch sein aber jemand der das erste Mal an einen neuen Ort kommt nimmt diesen ganz einfach anders wahr, sieht Details die der Gastgeber bereits seit Ewigkeiten nicht mehr wahrnimmt oder noch nie wahrgenommen hat und/oder stellt Fragen die einem die eigene Heimat nochmal ganz anders vor Augen führt.

# Und der/als Gast #
Die Vorteile für den Reisenden sind natürlich dementsprechend schnell und einfach aufgezählt: 

* kostenlose Unterkunft
* Tipps von Einheimischen um die Gegend/Stadt anders als typische Touristen zu erkunden
* eine Art Zuhause weit weg vom richtigen Zuhause 
* und allgemein der kulturelle Austausch

Meine schönsten und skurrilsten Momente hatte ich bei Studenten in Yogyakarta in Indonesien. Ich glaub am Ende war ich eine runde Woche bei meinem Gastgeber und hab zusammen mit ihm auf dem Boden gepennt im gleichen 6-8 m² Raum. Im zweiten Zimmer, gab es ein Wasserhahn mit einem Schlauch dran und ein Loch im Boden. Es war Klo, Dusche und Waschbecken in einem. Zusätzlich verdanke ich ihm mein günstigstes Essen meines Lebens. Zwei volle Teller leckerstes Nasi Goreng mit zwei Eistee für weniger als **einen** Euro und es war super lecker! Tisch und Stühle gab es nicht, sodass wir auf dem Bordstein direkt an der Straße Platz nahmen. Ja die Eindrücke aus Indonesien werden für immer in meinem Herzen bleiben und ganz besonders auch dank Couchsurfing!  
erwähnenswert sind auch meine Schlafplätze in Budapest, Cairns, Porto und Manaus, denn bei jedem der dortigen Gastgeber war ich mit bis zu fünf Gästen aus der ganzen Welt zusammen untergebracht. Meine Erinnerung an Fernando in Porto ist eine ganz spezielle, denn er ist bisher nach wie vor das einzige CS-Mitglied, das ich bisher ein zweites Mal getroffen hab. Schaut man sich sein [Profil](https://www.couchsurfing.com/people/openness) an, könnte man denken das er schon sein ganzes Leben für die runden 3000 Übernachtungen von rund 2000 verschiedenen Gästen die ihn bereits besucht haben, benötigt hat. Dabei gibt es CS doch erst seit 2003.

{% include figure image_path="/assets/images/posts/couchsurfing/budapest.jpg" id="budapest" alt="Ein Platzhalter, falls das Bild nicht gefunden wird." caption="Volles Haus bei meinem Gastgeber in Budapest 2013. **©** Bastian Gülstorf" %}

# Historisches + Tipps/Etikette #
Mir ist durchaus bewusst das die Idee, Fremde zu sich nach Hause einzuladen wahrscheinlich so alt ist wie die Menschheit selber. Außerdem kann ich mir gut vorstellen, das bevor man anfing über das Internet zu kommunizieren, man in der analogen Welt wahrscheinlich Bücher hatte mit Einträgen zu hilfsbereiten Personen aus der ganzen Welt. Ähnliches kenne ich zum Beispiel von WWOOFing[^WWOOF]. Es würde vielleicht sogar noch heute funktionieren, nur der Umstand einen richtigen Brief in z.B. ein fernes Land zu schicken und auf eine positive Antwort zu hoffen, würde wahrscheinlich die meisten in unserer schnelllebigen Gesellschaft heutzutage zermürben.  
Ja die Moderne hat ihre Berechtigung aber es hat noch niemand geklagt wenn er eine nette sowie personalisierte Nachricht von einem möglichen Gast bekommen hat. Statt also Kopien einer Ursprungs-Anfrage an möglichst viele Gastgeber zu schicken, sollte es eigentlich reichen zwei-drei individuelle Nachrichten zu schicken um einen Gastgeber zu finden, denn andersherum möchte man als Gastgeber ja auch nicht einfach nur als kostenlose Absteige dastehen. Zumindest ich kann mir das nicht vorstellen und bevorzuge einfach richtige Gespräche statt eines Austausches von Standard-Floskeln.  
Ein weiterer positiver Nebeneffekt ist, das man ohne Scheu seine Sprachfähigkeiten trainieren kann. Mein Englisch hatte damals mit den ersten Gästen nochmal einen ordentlichen Schub bekommen, genauso wie mein Portugiesisch beim Reisen am Amazonas. 

# Aktuelles # 
Klar gab es bei Couchsurfing auch immer schon Leute, die nicht wirklich reinpassen wollten, die statt Vollkontakt mit einer neuen Kultur lieber ein sanftes Kennenlernen bevorzugen und bereit sind dafür auch zu bezahlen. Seitdem es die Plattform Airbnb gibt, hat sich glücklicherweise ein gewisser Teil der CS-Gemeinschaft abgelöst. Es hat dem CS-Erlebnis sicher nicht geschadet. Wer lieber gar kein Kontakt mit lokalen Leuten sucht, bucht sich ein Hotel. Wer gerne lokales probiert aber trotzdem auf Nummer sicher geht, wird wahrscheinlich Airbnb nutzen und die, die Abenteuer mögen, werden lieber ihrer Intuition folgend, CS nutzen.  
Um ehrlich zu sein, hab ich selber erst vor kurzem meinen Spaß an CS wiedergefunden. In meinem letzten Monat hier in Brasilien hatte sich zum Glück nochmal die Chance ergeben mit Ruhe einen Monat lang zu reisen. Dabei war ich insgesamt bei drei Gastgebern runde zwei Wochen untergebracht, bei denen ich alles das wiedergefunden habe, was ich so lange so schmerzlich hier in Brasilien vermisst hatte.  
In Manaus fand ich jemand mit dem ich entspannt bei ein paar Bier Geschichten/Ideen austauschen konnte. In Alter do Chão konnte ich endlich barfuß zu guter südamerikanischer Live-Musik in den Straßen durch die Nacht tanzen und in Belém schließlich konnte ich meinen Frieden mit Brasilien machen, durch nüchternes also wissenschaftliches Fakten-Vergleichen. All das konnte ich aber erst dank der Menschen, die mich aufgenommen haben und mir ihr Brasilien gezeigt haben oder zumindest ihre Stadt/Gegend. Daher hab ich mich auch entschieden, sobald wie möglich selber wieder Leute zu beherbergen.

Was bleibt zu sagen außer: happy surfing / hosting! :smiley:

[^hitchhiking]: Und seine Geschichten über das Reisen "per Anhalter".
[^no_duty]: Was natürlich keine Pflicht ist.
[^seteEum]: In Brasilien ist das 7:1 im Halbfinale der WM 2014 eine Legende, die so schnell nicht vergessen wird.
[^still_exist]: Referenzen anderer CS-Mitglieder sind nach wie vor das sicherste Indiz um jemand ernsthaft einschätzen zu wollen. Also ob man Vertrauen schenken kann oder lieber sein lassen sollte.
[^special_dish]: Das es sonst nur in einer kleinen Ecke Rumäniens zu Essen gibt.
[^WWOOF]: **W**orld-**W**ide **O**pportunities on **O**rganic **F**arms

*[CS]: Couchsurfing

[comment]: <> (Mögliche künftige Referenzen: WWOOFing, tramping, ..)
